import java.util.Scanner;

public class Application3{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a string please:");
        String s = input.nextLine();
        System.out.println("You've entered string " + s);
        input.close();
    }
}