import java.util.Scanner;

public class Calculate{
    public static void main(String[] args){
        int op, num1, num2, result;
        Scanner scanner = new Scanner(System.in);
        System.out.println("1、+\n2、-\n3、*\n4、/\n请输入操作选项：");
        op = scanner.nextInt();
        System.out.println("请输入两个数：");
        num1 = scanner.nextInt();
        num2 = scanner.nextInt();
        char c = (op==1)?'+':((op==2)?'-':((op==3)?'*':((op==4)?'/':' ')));
        result = (op==1)?(num1+num2):((op==2)?(num1-num2):((op==3)?(num1*num2):((op==4)?(num1/num2):' ')));
        if(c != ' ')
            System.out.println(num1+"" + c + num2 + "=" + result);
        else
            System.out.println("选项错误计算失败");
    }
}