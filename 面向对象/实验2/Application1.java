import java.awt.*;
import java.awt.event.*;

public class Application1{
    public static void main(String[] args){
        new FrameInOut();
    }
}
class FrameInOut extends Frame implements ActionListener{
    Label prompt;
    TextField input, output;
    Button button;
    public FrameInOut(){
        super("??��????Java Application????");
        prompt = new Label("???????????????");
        input = new TextField(6);
        output = new TextField(20);
        button = new Button("???");
        setLayout(new FlowLayout());
        add(prompt);
        add(input);
        add(output);
        add(button);
        input.addActionListener(this);
        button.addActionListener(this);
        setSize(300, 200);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == input)
            output.setText(input.getText() + "???????!");
        else{
            dispose();
            System.exit(0);
        }
    }
}