#include <stdio.h>
int main(){
    int op, num1, num2, result;
    char c;
    printf("1、+\n2、-\n3、*\n4、/\n请输入操作选项：");
    scanf("%d", &op);
    printf("请输入两个数：");
    scanf("%d %d", &num1, &num2);
    c = (op==1)?'+':((op==2)?'-':((op==3)?'*':((op==4)?'/':' ')));
    result = (op==1)?(num1+num2):((op==2)?(num1-num2):((op==3)?(num1*num2):((op==4)?(num1/num2):' ')));
    if(op != ' ')
        printf("%d%c%d=%d", num1, c, num2, result);
    else
        printf("选择错误，计算失败");
}