#include <stdio.h>
#include <malloc.h>
// 函数结果状态码
#define OK 1
#define ERROR 0
// Status 是函数的类型，其值是函数结果状态代码
typedef int Status;
//定义ElemType数据类型
typedef char ElemType;
// 定义节点结构体
typedef struct LNode{
    ElemType data;
    struct LNode *next;
} LNode, *LinkList;

// 函数声明
Status ListInsert(LinkList, int , ElemType);
Status ListDelete(LinkList, int , ElemType*);
void printLinkList(LinkList);
// 主函数
int main(){
    LinkList L = (LinkList)malloc(sizeof(LNode));   // 创建链表头节点
    ElemType e; // 定义一个e，用来接收删除的数据
    int i;
    L->next = NULL; // 设置头结点的下一个节点为空
    for(i=0; i<26; i++){
        ListInsert(L, i+1, i+65);    //调用插入方法，将新节点数据插入到链表中
    }
    printLinkList(L);   // 调用打印链表方法查看插入后的结果
    i=5;    
    if(ListDelete(L, i, &e)){
        printf("删除第%d个节点，其值为%c\n", i, e);
    }else{
        printf("删除第%d个节点失败\n", i);
    }
    printLinkList(L);
    if(ListInsert(L, 20, e)){
        printf("节点%c插入成功\n", e);
    }else{
        printf("节点%c插入失败\n", e);
    }
    printLinkList(L);
    return 0;
}

// 函数定义
// 插入方法
Status ListInsert(LinkList L, int i, ElemType e){
    // 在带头节点的单链线性表L中第i个位置之前插入元素e
    LinkList p = L;
    int j = 0;
    // 寻找第i-1个节点
    while(p && j < i-1){
        p = p->next;
        ++j;
    }
    // i小于1或者大于表长加1
    if(!p || j > i-1)
        return ERROR;
    LNode *s = (LinkList) malloc(sizeof(LNode));
    s->data = e;
    s->next = p->next;
    p->next = s;
    return OK;
}

// 删除方法
Status ListDelete(LinkList L, int i, ElemType *e){
    // 在带头节点的单链线性表L中，删除第i个元素，并由e返回其值
    LinkList p = L;
    int j = 0;
    // 寻找第i个节点，并令p指向其前趋
    while(p->next && j<i-1){
        p = p->next;
        ++j;
    }
    // 删除位置不合理
    if(!(p->next)||j>i-1)
        return ERROR;
    // 删除并释放节点
    LinkList q = p->next;
    p->next = q->next;
    *e = q->data;
    free(q);
    return OK;
}

void printLinkList(LinkList L){
    LinkList p = L;
    int count = 0;
    while(p->next){
        p = p->next;
        printf("%5c", p->data);
        if(++count == 7){
            count = 0;
            printf("\n");
        }
    }
    printf("\n-------------------------------\n");
}