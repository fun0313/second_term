#include <stdio.h>
// 定义 Complex 结构体类型
// real 实部，virt虚部
typedef struct{
    int real, virt;
}Complex;

// 函数声明
void initComplexes(Complex *, Complex *);
void printComplex(Complex);
void printResult(Complex, Complex, char);
Complex initComplex(int, int);
Complex add(Complex, Complex);
Complex sub(Complex,Complex);
Complex mul(Complex, Complex);
int getVirt(Complex);
int getReal(Complex);

//主程序
int main(){
    Complex complex1, complex2;
    initComplexes(&complex1,&complex2);
    printResult(complex1, complex2,'+');
    printResult(complex1, complex2,'-');
    printResult(complex1, complex2,'*');
    return 0;
}

// 初始化两个复数
void initComplexes(Complex *c1, Complex *c2){
    printf("请输入第一个复数的实部：");
    scanf("%d",&c1->real);
    printf("请输入第一个复数的虚部：");
    scanf("%d", &c1->virt);
    printf("请输入第二个复数的实部：");
    scanf("%d",&c2->real);
    printf("请输入第二个复数的虚部：");
    scanf("%d", &c2->virt);
    printf("\n");
}

// 初始化一个复数
// @real 实部
// @virt 虚部
// @return 新的复数
Complex initComplex(int real, int virt){
    Complex complex;
    complex.real = real;
    complex.virt = virt;
    return complex;
}

// 打印复数需要区分0的情况
// @complex 复数参数
void printComplex(Complex complex){
    int real = getReal(complex);
    int virt = getVirt(complex);
    if(real == 0 && virt == 0){
        printf("0");
    }else if(real == 0 || virt == 0){
        if(real != 0)
            printf("%d", real);
        else if(real == 0){
            printf("%d%c", virt, 'i');
        }
    }else{
        printf("(%d%c%d%c)", real, (virt>0)?'+':'\0', virt, 'i');
    }
}

// 打印表达式
// @c1 第一个复数
// @c2 第二个复数
// @op 运算符
void printResult(Complex c1, Complex c2, char op){
    printComplex(c1);
    printf("%c", op);
    printComplex(c2);
    printf("=");
    if(op == '+'){
        printComplex(add(c1, c2));
    }else if(op == '-'){
        printComplex(sub(c1, c2));
    }else if(op == '*'){
        printComplex(mul(c1, c2));
    }
    printf("\n");
}

//计算两个复数和
// @c1 第一个复数
// @c2 第二个复数
// @return 复数和
Complex add(Complex c1, Complex c2){
    return initComplex(c1.real+c2.real, c1.virt+c2.virt);
}

// 计算两个复数差
// @c1 第一个复数
// @c2 第二个复数
// @return 复数差
Complex sub(Complex c1,Complex c2){
    return initComplex(c1.real-c2.real, c1.virt-c2.virt);
}

// 计算两个复数乘积
// @c1 第一个复数
// @c2 第二个复数
// @return 复数积
Complex mul(Complex c1, Complex c2){
    return initComplex(c1.real*c2.real-c1.virt*c2.virt, c1.virt*c2.real + c1.real*c2.virt);
}

// 获取已知复数虚部
// @c 复数
// @return 返回复数虚部
int getVirt(Complex c){
    return c.virt;
}

// 获取已知复数实部
// @c 复数
// @return 返回复数实部
int getReal(Complex c){
    return c.real;
}
